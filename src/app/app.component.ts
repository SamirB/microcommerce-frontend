import {Component, OnInit} from '@angular/core';
import { take } from 'rxjs/operators';
import {interval, Observable, ObservableInput, Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  secondes: number;
  counterSubscription: Subscription;

  constructor() {}

  ngOnInit() {
    const counter = interval(1000);
    this.counterSubscription = counter.subscribe(
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log("Il y a une erreur")
      },
      () => {
        console.log("Observable terminé");
      }
    );

  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe();
  }

}
