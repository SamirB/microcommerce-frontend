import {Subject} from "rxjs";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class AppareilService {

  appareilsSubject = new Subject();
  lstT = [];

  private appareils = [];

  emitAppareilSubject(): void {

    this.appareilsSubject.next(this.appareils.slice());
  }

  /**
   *
   * @param test Juste a string test
   * @return random string
   */
  testComment (test: String){
    return "";

}
  switchOnAll() {
    for(let appareil of this.appareils) {
      appareil.status = 'allumé';
      this.emitAppareilSubject();
    }

  }
  switchOnOne(i: number) {
    this.appareils[i].status = 'allumé';
    this.emitAppareilSubject();
  }

  switchOffAll() {
    for(let appareil of this.appareils) {
      appareil.status = 'éteint';
      this.emitAppareilSubject();
    }
  }

  switchOffOne(i: number) {
    this.appareils[i].status = 'éteint';
    this.emitAppareilSubject();
  }
  getAppareilById(id: number) {
    const appareil = this.appareils.find(
      (s) => {
        return s.id === id;
      }
    );
    return appareil;
  }

  addAppareil(name: string, status: string) {
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.appareils[(this.appareils.length - 1)].id + 1;
    this.appareils.push(appareilObject);
    this.emitAppareilSubject();
  }

  constructor(private httpClient: HttpClient) { }

  // test(){
  //
  //
  //   this.httpClient
  //     .get<any[]>('http://localhost:8000/Produits')
  //     .subscribe(
  //       (response) => {
  //         console.log(" localhost:8000 OK*********"+response);
  //         },
  //       (error) => {
  //         console.log('Erreur ! : ' + error);
  //       }
  //     );
  //
  // }

  saveAppareilsToServer() {
    this.httpClient
      .put('https://microcommerce-backend.firebaseio.com/appareils.json', this.appareils)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getAppareilsFromServer() {
    this.httpClient
      .get<any[]>('https://microcommerce-backend.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          this.appareils = response;
          this.emitAppareilSubject();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

}
