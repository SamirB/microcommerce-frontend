import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppareilComponent } from './appareil/appareil.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';


//Services / Providers
import { AppareilService } from './services/appareil.service';
import {AuthService} from "./services/auth.service"; //Connection service
import {AuthGuard} from "./services/auth-guard.service"; //Secure URLs

// Content page
import { SingleappareilComponent } from './singleappareil/singleappareil.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthComponent } from './auth/auth.component';
import { AppareilViewComponent } from './appareil-view/appareil-view.component';
import { EditAppareilComponent } from './edit-appareil/edit-appareil.component';
import {UserService} from "./services/users.service";
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';
import {HttpClientModule} from "@angular/common/http";





const appRoutes: Routes = [
  { path: '', component: AppareilViewComponent },//Return to the home page
  { path: 'auth', component: AuthComponent },//Login page
  { path: 'appareils',canActivate: [AuthGuard], component: AppareilViewComponent }, //Home page
  { path: 'appareils/:id',canActivate: [AuthGuard], component: SingleappareilComponent },//Detail page
  { path: 'edit', component: EditAppareilComponent  },//Edit page
  { path: 'users', component: UserListComponent  },//Users page
  { path: 'new-user', component: NewUserComponent },// Create User page




  //Error page
  { path: 'not-found', component: FourOhFourComponent },//Error page
  { path: '**', redirectTo: 'not-found' }//Redirect all wrong URls to the not-found page
];

@NgModule({
  declarations: [
    AppComponent,
    AppareilComponent,
    AuthComponent,
    AppareilViewComponent,
    SingleappareilComponent,
    FourOhFourComponent,
    EditAppareilComponent,
    UserListComponent,
    NewUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AppareilService,
    AuthService,
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
